import torch
from torch.utils.data import Dataset
from scipy.io import loadmat
from scipy.fft import fft
import numpy as np
from scipy.signal import hilbert

class MyDataSet(Dataset):
    """自定义数据集"""

    def __init__(self, images_path: list, images_class: list, transform=None):
        self.images_path = images_path
        self.images_class = images_class
        self.transform = transform

    def __len__(self):
        return len(self.images_path)

    def __getitem__(self, item):
        eeg_data = loadmat(self.images_path[item])

        label = self.images_class[item]
        eeg_data = eeg_data["data"]

        fft_data = np.empty((1, 64), dtype=np.complex64)

        fft_data[0, :] = hilbert(eeg_data[0, :])
        # fft_data[0, :] = fft(eeg_data[0, :])
        if self.transform is not None:
            fft_data = self.transform(fft_data)
            eeg_data = self.transform(eeg_data)
        # fft_data = torch.abs(fft_data)

        '''imfs_a = np.stack(eeg_data, axis=0).reshape(4, 64)

        hilbert_imfs = [hilbert(i) for i in imfs_a]
        hilbert_imfs_a = np.stack(hilbert_imfs, axis=0).astype(np.complex64)'''


        return fft_data, label

    @staticmethod
    def collate_fn(batch):
        images, labels = tuple(zip(*batch))

        images = torch.stack(images, dim=0)
        labels = torch.as_tensor(labels)
        return images, labels
