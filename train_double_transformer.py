import os
import math
import argparse
import torch
import torch.nn as nn
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms
import torchvision.models as models
from my_dataset import MyDataSet
from double_transformer import DoubleTransformer as create_model
from utils import read_split_data, train_one_epoch, evaluate
import pandas as pd
import warnings
from torchstat import stat
from thop import profile

warnings.filterwarnings("ignore")

def main(args):
    device = torch.device(args.device if torch.cuda.is_available() else "cpu")
    if os.path.exists("./weights") is False:
        os.makedirs("./weights")

    model = create_model(num_classes=args.num_classes).to(device)
    input = torch.randn(16, 1, 1, 64).cuda()
    flops, params = profile(model, inputs=(input,))
    print('flops:{}'.format(flops))
    print('params:{}'.format(params))

    train_images_path, train_images_label, val_images_path, val_images_label = read_split_data(args.data_path)

    data_transform = {
        "train": transforms.Compose([
            transforms.ToTensor(),
        ]),
        "val": transforms.Compose([
            transforms.ToTensor(),
        ])
    }
    # 实例化训练数据集
    train_dataset = MyDataSet(images_path=train_images_path,
                              images_class=train_images_label,
                              transform=data_transform["train"])

    # 实例化验证数据集
    val_dataset = MyDataSet(images_path=val_images_path,
                            images_class=val_images_label,
                            transform=data_transform["val"])

    batch_size = args.batch_size
    nw = min([os.cpu_count(), batch_size if batch_size > 1 else 0, 8])  # number of workers
    print('Using {} dataloader workers every process'.format(nw))
    train_loader = torch.utils.data.DataLoader(train_dataset,
                                               batch_size=batch_size,
                                               shuffle=True,
                                               pin_memory=True,
                                               num_workers=nw,
                                               collate_fn=train_dataset.collate_fn)

    val_loader = torch.utils.data.DataLoader(val_dataset,
                                             batch_size=batch_size,
                                             shuffle=False,
                                             pin_memory=True,
                                             num_workers=nw,
                                             collate_fn=val_dataset.collate_fn)

    # optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9, weight_decay=5E-5)
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=5E-6)
    lf = lambda x: ((1 + math.cos(x * math.pi / args.epochs)) / 2) * (1 - args.lrf) + args.lrf  # cosine
    scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lf)

    results = []

    for epoch in range(args.epochs):
        # train
        train_loss, train_acc, train_precision, train_sensitivity, train_f1, train_specificity = train_one_epoch(model=model,
                                                                                                                 optimizer=optimizer,
                                                                                                                 data_loader=train_loader,
                                                                                                                 device=device,
                                                                                                                 epoch=epoch)

        scheduler.step()

        # validate
        val_loss, val_acc, val_precision, val_sensitivity, val_f1, val_specificity = evaluate(model=model,
                                                                                              data_loader=val_loader,
                                                                                              device=device,
                                                                                              epoch=epoch)
        results.append({
            'epoch': epoch,
            'train_loss': train_loss,
            'train_acc': train_acc,
            'train_precision': train_precision,
            'train_sensitivity': train_sensitivity,
            'train_f1': train_f1,
            'train_specificity': train_specificity,
            'val_loss': val_loss,
            'val_acc': val_acc,
            'val_precision': val_precision,
            'val_sensitivity': val_sensitivity,
            'val_f1': val_f1,
            'val_specificity': val_specificity,
            'learn_rate': optimizer.param_groups[0]["lr"]
        })

        df = pd.DataFrame(results)

        # 保存为Excel文件
        df.to_excel('training_double_transformer_hht5.xlsx', index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--num_classes', type=int, default=5)
    parser.add_argument('--epochs', type=int, default=50)
    parser.add_argument('--batch-size', type=int, default=16)
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--lrf', type=float, default=0.000001)

    parser.add_argument('--data-path', type=str,
                        default="./data_storage/")
    parser.add_argument('--model-name', default='', help='create model name')

    # 预训练权重路径，如果不想载入就设置为空字符
    parser.add_argument('--weights', type=str, default='',
                        help='initial weights path')
    parser.add_argument('--device', default='cuda', help='device id (i.e. 0 or 0,1 or cpu)')

    opt = parser.parse_args()

    main(opt)
