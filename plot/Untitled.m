%% 画很多条折线图
% 读取Excel表格
filename = 'E:\computer_program\pycharm\rotating_arc_sensor\result\all_版本1_min-max标准化.xlsx';
data = xlsread(filename);

num_rows = size(data, 1);

% 生成随机索引
random_indices = randperm(num_rows);

% 选择前8行
selected_rows = data(random_indices(1:16), 1:64);

% 假设您的数据是一个8x64的数组，将其替换为实际的数据

% 生成横向间隔
z_offsets = 1:1:16; % 每两个平面之间的间隔为1

% 生成X轴的坐标
x_values = repmat((1:64)', 1, 16);

% 绘制三维图
figure;
for i = 1:16
    plot3(x_values(:, i), repmat(z_offsets(i), 64, 1), selected_rows(i, :),  'LineWidth', 1.7);
    hold on;
end

% 添加标签和标题
xlabel('时间');
ylabel('信号');
zlabel('幅值');

% 显示网格
grid on;

%% 画包络线
% 指定Excel文件路径
filename = 'E:\project\paper\毕业设计\material\train_result\Deepconvnet_Accuracy.xlsx'; % 请替换为您的Excel文件路径

% 读取Excel文件，假设数据从第二行开始（跳过表头）
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);

% 提取第二列（迭代折线），第四列和第五列（误差范围）
iteration_line = data{:, 2}; % 迭代折线
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 创建迭代次数向量
iterations = 1:length(iteration_line);

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], ...
     [0.9 0.9 0.9], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5

hold on;

% 绘制迭代折线
plot(iterations, iteration_line, 'b', 'LineWidth', 2, 'Color', [0 0 0.5]); % 使用深蓝色

% 添加图例和标签
legend('Error Envelope', 'Iteration Line');
xlabel('Iteration');
ylabel('Value');
title('Iteration Line with Error Envelope');

% 设置图形界面
grid on;
hold off;


%%
iterations = 1:49;

filename = 'E:\project\paper\毕业设计\material\train_result\MLP_accuracy.xlsx'; % 请替换为您的Excel文件路径
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [246/255, 111/255, 105/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

hold on;

filename = 'E:\project\paper\毕业设计\material\train_result\LSTM_accuracy.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [21/255, 151/255, 165/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\Deepconvnet_Accuracy.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [255/255, 194/255, 75/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\MLP_accuracy.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [246/255, 111/255, 105/255], 'DisplayName', 'MLP'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\LSTM_accuracy.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [21/255, 151/255, 165/255], 'DisplayName', 'LSTM'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\Deepconvnet_Accuracy.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [255/255, 194/255, 75/255], 'DisplayName', 'Deepconvnet'); % 使用深蓝色

legend('Location', 'southeast');
ax = gca;
yticks(ax, 0:0.1:1);  % Set y-ticks at desired intervals
yticklabels(ax, string((0:0.1:1)*100) + '%');  % Convert y-tick values to percentage format
xlabel('Epochs');
ylabel('Accuracy');
title('Validation Accuracy Curve');
grid on;
%%
iterations = 1:49;

filename = 'E:\project\paper\毕业设计\material\train_result\EEGnet_accuracy.xlsx'; % 请替换为您的Excel文件路径
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [54/255, 110/255, 207/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
hold on;

filename = 'E:\project\paper\毕业设计\material\train_result\resnet18_accuracy.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [191/255, 30/255, 46/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\Doubletransformer_accuracy.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [78/255, 171/255, 144/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\EEGnet_accuracy.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [54/255, 110/255, 207/255], 'DisplayName', 'EEGnet'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\resnet18_accuracy.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [191/255, 30/255, 46/255], 'DisplayName', 'Resnet18'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\Doubletransformer_accuracy.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [78/255, 171/255, 144/255], 'DisplayName', 'Ours'); % 使用深蓝色

legend('Location', 'southeast');
ax = gca;
yticks(ax, 0:0.1:1);  % Set y-ticks at desired intervals
yticklabels(ax, string((0:0.1:1)*100) + '%');  % Convert y-tick values to percentage format
ylim([0.2 1]);
xlabel('Epochs');
ylabel('Accuracy');
title('Validation Accuracy Curve');
grid on;