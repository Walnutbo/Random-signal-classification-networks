%% accuracy and flops
% 定义数据
models = {'MLP', 'LSTM', 'Deepconvnet', 'EEGnet', 'Resnet18', 'Ours'};
flops = [8671232, 22237184, 19219200, 4330496, 512634880, 9163008];
accuracy = [78.24, 87.01, 92.98, 91.22, 96.14, 97.54];
accuracy_errors = [2.66, 2.00, 2.77, 0, 1.92, 0.96];
accuracy = accuracy/100;
accuracy_errors = accuracy_errors/100;
% 创建图表
figure;
errorbar(flops, accuracy, accuracy_errors, 'o');
set(gca, 'XScale', 'log'); % 将X轴设置为对数尺度

% 添加标签和标题
xlabel('FLOPs');
ylabel('Accuracy');
title('Model Performance');

% 添加数据点标签
text(flops, accuracy, models, 'VerticalAlignment','bottom', 'HorizontalAlignment','right')

ax = gca;
yticks(ax, 0:0.025:1);  % Set y-ticks at desired intervals
yticklabels(ax, string((0:0.025:1)*100) + '%');  % Convert y-tick values to percentage format
xlim([2000000 1000000000]);
% 显示网格
grid on;

% 显示图表
hold off;

