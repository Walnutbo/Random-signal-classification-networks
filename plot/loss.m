%%
iterations = 1:49;

filename = 'E:\project\paper\毕业设计\material\train_result\MLP_loss.xlsx'; % 请替换为您的Excel文件路径
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [246/255, 111/255, 105/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

hold on;

filename = 'E:\project\paper\毕业设计\material\train_result\LSTM_loss.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [21/255, 151/255, 165/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\Deepconvnet_loss.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [255/255, 194/255, 75/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\MLP_loss.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [246/255, 111/255, 105/255], 'DisplayName', 'MLP'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\LSTM_loss.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [21/255, 151/255, 165/255], 'DisplayName', 'LSTM'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\Deepconvnet_loss.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [255/255, 194/255, 75/255], 'DisplayName', 'Deepconvnet'); % 使用深蓝色

legend('Location', 'northeast');
xlabel('Epochs');
ylabel('Loss');
title('Validation Loss Curve');
ylim([0 2]);
grid on;
%%
iterations = 1:49;

filename = 'E:\project\paper\毕业设计\material\train_result\EEGnet_loss.xlsx'; % 请替换为您的Excel文件路径
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [54/255, 110/255, 207/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
hold on;

filename = 'E:\project\paper\毕业设计\material\train_result\resnet18_loss.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [191/255, 30/255, 46/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\Doubletransformer_loss.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
lower_envelope = data{:, 4}; % 下包络线
upper_envelope = data{:, 5}; % 上包络线

% 绘制包络线之间的填充区域
h = fill([iterations fliplr(iterations)], [upper_envelope' fliplr(lower_envelope')], [78/255, 171/255, 144/255], 'LineStyle', 'none');
set(h, 'FaceAlpha', 0.5); % 设置填充区域的透明度为0.5
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

filename = 'E:\project\paper\毕业设计\material\train_result\EEGnet_loss.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [54/255, 110/255, 207/255], 'DisplayName', 'EEGnet'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\resnet18_loss.xlsx';
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [191/255, 30/255, 46/255], 'DisplayName', 'Resnet18'); % 使用深蓝色

filename = 'E:\project\paper\毕业设计\material\train_result\Doubletransformer_loss.xlsx'; 
opts = detectImportOptions(filename, 'NumHeaderLines', 1);
data = readtable(filename, opts);
iteration_line = data{:, 2}; % 迭代折线
plot(iterations, iteration_line, 'LineWidth', 2, 'Color', [78/255, 171/255, 144/255], 'DisplayName', 'Ours'); % 使用深蓝色

legend('Location', 'northeast');
xlabel('Epochs');
ylabel('Loss');
title('Validation Loss Curve');
grid on;