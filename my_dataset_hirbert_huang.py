import torch
from torch.utils.data import Dataset
from scipy.io import loadmat
import pickle
import numpy as np
from scipy.signal import hilbert
from scipy.fft import fft
from PyEMD import EMD
from array import array

class MyDataSet(Dataset):
    """自定义数据集"""

    def __init__(self, images_path: list, images_class: list, transform=None):
        self.images_path = images_path
        self.images_class = images_class
        self.transform = transform

    def __len__(self):
        return len(self.images_path)

    def __getitem__(self, item):
        label = self.images_class[item]
        label = label
        eeg_data = loadmat(self.images_path[item])
        eeg_data = eeg_data["data"]

        emd = EMD()
        imfs = [emd(eeg_data[electrode, :], max_imf=3) for electrode in range(60)]

        imfs_a = np.stack(imfs, axis=0).reshape(60*4, 1000)

        hilbert_imfs = [hilbert(i) for i in imfs_a]
        hilbert_imfs_a = np.stack(hilbert_imfs, axis=0).astype(np.complex64)

        if self.transform is not None:
            hilbert_imfs_a = self.transform(hilbert_imfs_a)

        return hilbert_imfs_a, label

    @staticmethod
    def collate_fn(batch):
        images, labels = tuple(zip(*batch))

        images = torch.stack(images, dim=0)
        labels = torch.as_tensor(labels)
        return images, labels
