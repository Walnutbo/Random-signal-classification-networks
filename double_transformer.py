from functools import partial
from collections import OrderedDict
import torch.nn.functional as F
import torch
import math
import torch.nn as nn


class ComplexLayerNorm(nn.Module):
    def __init__(self, normalized_shape, eps=1e-5):
        super(ComplexLayerNorm, self).__init__()
        self.normalized_shape = normalized_shape
        self.eps = eps

        # Initialize learnable parameters
        self.weight_r = nn.Parameter(torch.ones(normalized_shape))
        self.weight_i = nn.Parameter(torch.ones(normalized_shape))
        self.bias_r = nn.Parameter(torch.zeros(normalized_shape))
        self.bias_i = nn.Parameter(torch.zeros(normalized_shape))

    def forward(self, input_tensor):
        # Assume input_tensor is a complex tensor with real and imaginary parts
        # input_tensor = input_tensor_r + i * input_tensor_i

        # Calculate mean and variance along each dimension
        mean_r = input_tensor.real.mean(dim=-1, keepdim=True)
        mean_i = input_tensor.imag.mean(dim=-1, keepdim=True)
        var_r = input_tensor.real.var(dim=-1, unbiased=False, keepdim=True)
        var_i = input_tensor.imag.var(dim=-1, unbiased=False, keepdim=True)

        # Normalize the input tensor
        input_normalized_r = (input_tensor.real - mean_r) / torch.sqrt(var_r + self.eps)
        input_normalized_i = (input_tensor.imag - mean_i) / torch.sqrt(var_i + self.eps)

        # Apply the learnable parameters
        output_r = self.weight_r * input_normalized_r + self.bias_r
        output_i = self.weight_i * input_normalized_i + self.bias_i

        # Combine real and imaginary parts
        output = torch.complex(output_r, output_i)

        return output


class DeepConv(nn.Module):
    def __init__(self, layers):
        super(DeepConv, self).__init__()

        self.conv1 = nn.Conv2d(2, 7, kernel_size=(1, 7), padding=(0, 2))
        self.conv2 = nn.Conv2d(7, 1, kernel_size=(1, 7), padding=(0, 2))
        self.batchnorm1 = nn.BatchNorm2d(7)
        self.batchnorm2 = nn.BatchNorm2d(1)

    def forward(self, x):
        x = F.relu(self.batchnorm1(self.conv1(x)))
        x = F.relu(self.batchnorm2(self.conv2(x)))
        return x


def drop_path(x, drop_prob: float = 0., training: bool = False):
    if drop_prob == 0. or not training:
        return x
    keep_prob = 1 - drop_prob
    shape = (x.shape[0],) + (1,) * (x.ndim - 1)  # work with diff dim tensors, not just 2D ConvNets
    random_tensor = keep_prob + torch.rand(shape, dtype=x.dtype, device=x.device)
    random_tensor.floor_()  # binarize
    output = x.div(keep_prob) * random_tensor
    return output


class DropPath(nn.Module):
    """
    Drop paths (Stochastic Depth) per sample  (when applied in main path of residual blocks).
    """
    def __init__(self, drop_prob=None):
        super(DropPath, self).__init__()
        self.drop_prob = drop_prob

    def forward(self, x):
        return drop_path(x, self.drop_prob, self.training)


class Attention(nn.Module):
    def __init__(self,
                 dim,   # 输入token的dim
                 num_heads=8,
                 qkv_bias=False,
                 qk_scale=None,
                 attn_drop_ratio=0.,
                 proj_drop_ratio=0.):
        super(Attention, self).__init__()
        self.num_heads = num_heads
        head_dim = dim // num_heads
        self.scale = qk_scale or head_dim ** -0.5
        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.attn_drop = nn.Dropout(attn_drop_ratio)
        self.proj = nn.Linear(dim, dim)
        self.proj_drop = nn.Dropout(proj_drop_ratio)

    def forward(self, x):
        # [batch_size, num_patches + 1, total_embed_dim]
        B, N, C = x.shape
        qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
        # [batch_size, num_heads, num_patches + 1, embed_dim_per_head]
        q, k, v = qkv[0], qkv[1], qkv[2]  # make torchscript happy (cannot use tensor as tuple)

        attn = (q @ k.transpose(-2, -1)) * self.scale
        attn = attn.softmax(dim=-1)
        attn = self.attn_drop(attn)

        x = (attn @ v).transpose(1, 2).reshape(B, N, C)
        x = self.proj(x)
        x = self.proj_drop(x)
        return x


class Block(nn.Module):
    def __init__(self,
                 dim,
                 layers,
                 num_heads,
                 mlp_ratio=2.,
                 qkv_bias=False,
                 qk_scale=None,
                 drop_ratio=0.,
                 attn_drop_ratio=0.,
                 act_layer=nn.GELU,
                 norm_layer=None):
        '''nn.LayerNorm'''
        super(Block, self).__init__()


        if layers == 1:
            self.norm1 = norm_layer(dim)
            self.norm2 = norm_layer(dim)
            self.norm3 = norm_layer(dim)
            self.norm4 = norm_layer(dim)
            self.attn1 = Attention(dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                   attn_drop_ratio=attn_drop_ratio, proj_drop_ratio=drop_ratio)
            self.attn2 = Attention(dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                   attn_drop_ratio=attn_drop_ratio, proj_drop_ratio=drop_ratio)
            self.conv1 = nn.Conv1d(64, 64, kernel_size=3, padding=1)
            self.conv2 = nn.Conv1d(64, 64, kernel_size=3, padding=1)
            self.linear1 = nn.Linear(64, 64)
            self.linear2 = nn.Linear(64, 64)
            self.linear3 = nn.Linear(64, 60)
            self.linear4 = nn.Linear(64, 60)
            self.norm5 = norm_layer(64)
            self.norm6 = norm_layer(64)
            self.conv3 = nn.Conv1d(2, 4, kernel_size=7, padding=3)
            self.conv4 = nn.Conv1d(2, 4, kernel_size=7, padding=3)
            self.linear5 = nn.Linear(2, 4)
            self.linear6 = nn.Linear(2, 4)
        else:
            self.norm1 = norm_layer(dim-4)
            self.norm2 = norm_layer(dim-4)
            self.norm3 = norm_layer(dim-4)
            self.norm4 = norm_layer(dim-4)
            self.attn1 = Attention(dim-4, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                   attn_drop_ratio=attn_drop_ratio, proj_drop_ratio=drop_ratio)
            self.attn2 = Attention(dim-4, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                   attn_drop_ratio=attn_drop_ratio, proj_drop_ratio=drop_ratio)
            self.conv1 = nn.Conv1d(60, 60, kernel_size=3, padding=1)
            self.conv2 = nn.Conv1d(60, 60, kernel_size=3, padding=1)
            self.linear1 = nn.Linear(60, 60)
            self.linear2 = nn.Linear(60, 60)
            self.linear3 = nn.Linear(60, 56)
            self.linear4 = nn.Linear(60, 56)
            self.norm5 = norm_layer(60)
            self.norm6 = norm_layer(60)
            self.conv3 = nn.Conv1d(4, 8, kernel_size=7, padding=3)
            self.conv4 = nn.Conv1d(4, 8, kernel_size=7, padding=3)
            self.linear5 = nn.Linear(4, 8)
            self.linear6 = nn.Linear(4, 8)


        self.deepconv1 = DeepConv(layers=layers)
        self.deepconv2 = DeepConv(layers=layers)


    def forward(self, x):
        a = torch.abs(x)
        b = torch.angle(x)
        a = a + self.attn1(self.norm1(a))
        b = b + self.attn2(self.norm3(b))

        a_flatten1 = self.norm5(self.conv1(a.transpose(1, 2)).transpose(1, 2)).unsqueeze(dim=1)

        a = a.unsqueeze(dim=1)
        b_flatten1 = self.norm6(self.conv2(b.transpose(1, 2)).transpose(1, 2)).unsqueeze(dim=1)

        b = b.unsqueeze(dim=1)

        a = self.linear1(a)
        b = self.linear2(b)

        a_temp = torch.cat((a, b_flatten1), dim=1)
        b_temp = torch.cat((b, a_flatten1), dim=1)

        a = a.squeeze(dim=1)
        b = b.squeeze(dim=1)

        a = self.linear3(a) + self.deepconv1(a_temp).squeeze(dim=1)
        b = self.linear4(b) + self.deepconv2(b_temp).squeeze(dim=1)

        a_flatten2 = self.conv3(a)
        b_flatten2 = self.conv4(b)

        a = self.linear5(a.transpose(1, 2)).transpose(1, 2) + b_flatten2
        b = self.linear6(b.transpose(1, 2)).transpose(1, 2) + a_flatten2

        x = torch.complex(a, b)
        return x


class LearnableFourierPositionalEncoding(nn.Module):
    def __init__(self, num_patches, embedding_dim):
        super(LearnableFourierPositionalEncoding, self).__init__()
        self.embedding_dim = embedding_dim

        # Positional embeddings
        self.position_embeddings = nn.Embedding(num_patches, embedding_dim)

        # Learnable Fourier weights
        self.sinusoidal_weights = nn.Parameter(torch.randn(embedding_dim // 2))
        self.cosine_weights = nn.Parameter(torch.randn(embedding_dim // 2))

    def forward(self, x):
        batch_size, num_patches, embedding_dim = x.size()

        # Generate positional embeddings
        positions = torch.arange(num_patches, device=x.device).unsqueeze(0).expand(batch_size, -1)
        position_embedded = self.position_embeddings(positions)

        # Generate Fourier positional encodings
        frequencies = torch.arange(embedding_dim // 2, device=x.device) * 2 * math.pi / embedding_dim
        sinusoidal_encodings = torch.sin(frequencies * self.sinusoidal_weights.view(1, -1))
        cosine_encodings = torch.cos(frequencies * self.cosine_weights.view(1, -1))
        encodings = torch.cat([sinusoidal_encodings, cosine_encodings], dim=-1)
        encodings = encodings.unsqueeze(0).expand(batch_size, -1, -1)

        # Add positional embeddings and Fourier encodings to the input
        return x + position_embedded + encodings


class VisionTransformer(nn.Module):
    def __init__(self, num_classes=1000, embed_dim=768, num_heads=12,
                 mlp_ratio=4.0, qkv_bias=True, qk_scale=None, drop_ratio=0.,
                 attn_drop_ratio=0., norm_layer=None, act_layer=None):

        super(VisionTransformer, self).__init__()
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        self.num_tokens = 1
        norm_layer = norm_layer or partial(nn.LayerNorm, eps=1e-7)
        act_layer = act_layer or nn.GELU

        num_patches = 1
        self.cls_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.pos_embed = LearnableFourierPositionalEncoding(num_patches=num_patches+1, embedding_dim=embed_dim)
        self.pos_drop = nn.Dropout(p=drop_ratio)

        self.blocks1 = Block(dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                  drop_ratio=drop_ratio, attn_drop_ratio=attn_drop_ratio,
                  norm_layer=norm_layer, act_layer=act_layer, layers=1)
        self.blocks2 = Block(dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                  drop_ratio=drop_ratio, attn_drop_ratio=attn_drop_ratio,
                  norm_layer=norm_layer, act_layer=act_layer, layers=2)

        self.norm = ComplexLayerNorm(normalized_shape=56)

        # Representation layer
        self.pre_logits = nn.Identity()

        self.fc = nn.Linear(112, num_classes)

        self.soft = nn.Softmax(dim=0)

        nn.init.trunc_normal_(self.cls_token, std=0.02)
        self.apply(_init_vit_weights)

    def forward(self, x):
        x = x.squeeze(dim=1).float()
        cls_token = self.cls_token.expand(x.shape[0], -1, -1)

        x = torch.cat((cls_token, x), dim=1)  # [B, 197, 768]
        x = self.pos_drop(self.pos_embed(x))

        x = self.blocks1(x)
        x = self.blocks2(x)

        x = self.norm(x)
        x = self.pre_logits(x[:, 0])
        real_x = x.real
        imag_x = x.imag
        x = torch.cat([real_x, imag_x], dim=-1)
        x = self.fc(x)

        return x


def _init_vit_weights(m):
    """
    ViT weight initialization
    :param m: module
    """
    if isinstance(m, nn.Linear):
        nn.init.trunc_normal_(m.weight, std=.01)
        if m.bias is not None:
            nn.init.zeros_(m.bias)
    elif isinstance(m, nn.Conv2d):
        nn.init.kaiming_normal_(m.weight, mode="fan_out")
        if m.bias is not None:
            nn.init.zeros_(m.bias)
    elif isinstance(m, nn.LayerNorm):
        nn.init.zeros_(m.bias)
        nn.init.ones_(m.weight)


def DoubleTransformer(num_classes: int = 1000):
    model = VisionTransformer(embed_dim=64,
                              num_heads=4,
                              num_classes=num_classes)
    return model

'''size = (16, 1, 1, 64)

real_part = torch.zeros(size)

imag_part = torch.zeros(size)

a = torch.complex(real_part, imag_part)

model = DoubleTransformer(num_classes=5)
b = model(a)'''